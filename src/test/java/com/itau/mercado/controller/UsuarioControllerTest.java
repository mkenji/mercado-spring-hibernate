package com.itau.mercado.controller;


import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.mercado.model.Usuario;
import com.itau.mercado.repository.UsuarioRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UsuarioRepository usuarioRepository;

	private JacksonTester<Usuario> jsonUsuario;

	@Before
	public void setup() {
		// Initializes the JacksonTester
		JacksonTester.initFields(this, new ObjectMapper());
		System.out.println(jsonUsuario);
	}


	@Test
	public void deveCriarNovoUsuario() throws Exception {
//		// given
//		given(usuarioRepository.getUsuario(2))
//		.willReturn(new Usuario(1, "Mannon", "RobotMan"));
//
//		// when
//		MockHttpServletResponse response = mvc.perform(
//				get("/usuario/cadastrar")
//				.accept(MediaType.APPLICATION_JSON))
//				.andReturn().getResponse();
//
//		// then
//		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
//		assertThat(response.getContentAsString()).isEqualTo(
//				jsonUsuario.write(new Usuario(1, "Mannon", "RobotMan")).getJson()
//				);
	}


}
