package com.itau.mercado.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.itau.mercado.model.Produto;
import com.itau.mercado.repository.ProdutoRepository;


@Controller
public class ProdutoController {
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/produto", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Produto> getProduto(){
		return produtoRepository.findAll();
	}
	
	@RequestMapping(path="/produto/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Produto> getProdutoById(@PathVariable(value="id") int id){
		return produtoRepository.findById(id);
	}
	
	@RequestMapping(path="/produto", method=RequestMethod.POST)
	@ResponseBody
	public Produto setProduto(@RequestBody Produto produto){
		return produtoRepository.save(produto);
	}
}
