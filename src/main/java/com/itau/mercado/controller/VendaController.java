package com.itau.mercado.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Produto;
import com.itau.mercado.model.Venda;
import com.itau.mercado.repository.ProdutoRepository;
import com.itau.mercado.repository.VendaRepository;

@Controller
public class VendaController {
	@Autowired
	VendaRepository vendaRepository;
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/venda", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Venda> getProduto(){
		return vendaRepository.findAll();
	}
	
	@RequestMapping(path="/venda/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Venda> getProdutoById(@PathVariable(value="id") int id){
		return vendaRepository.findById(id);
	}
	
	@RequestMapping(path="/venda", method=RequestMethod.POST)
	@ResponseBody
	public String setProduto(@RequestBody Venda venda){
		int produto_id = venda.getProduto().getId();
		Produto produto = produtoRepository.findById(produto_id).get();
		String mensagemRetorno = "";
		if(produto.getQuantidade() >= venda.getQuantidade_venda()) {
			produto.setQuantidade(produto.getQuantidade()-venda.getQuantidade_venda());
			venda.setValor_venda(produto.getPreco());
			vendaRepository.save(venda);
			produtoRepository.save(produto);
			mensagemRetorno = "Venda realizada com sucesso";
		} else {
			mensagemRetorno = "Venda não autorizada. Produto "+produto.getNome()+" sem estoque";
		}
		return mensagemRetorno;
		
	}
}
