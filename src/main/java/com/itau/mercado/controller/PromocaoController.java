package com.itau.mercado.controller;

import java.awt.List;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Produto;
import com.itau.mercado.model.Promocao;
import com.itau.mercado.repository.ProdutoRepository;
import com.itau.mercado.repository.PromocaoRepository;

@Controller
public class PromocaoController {
	@Autowired
	PromocaoRepository promocaoRepositoty;
	@Autowired
	ProdutoRepository produtoRepositoty;

	@RequestMapping(path="/promocao", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Promocao> getPromocao(){
		return promocaoRepositoty.findAll();
	}

	@RequestMapping(path="/promocao/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Promocao> getPromocaoById(@PathVariable(value="id") int id){
		return promocaoRepositoty.findById(id);
	}

	@RequestMapping(path="/promocao", method=RequestMethod.POST)
	@ResponseBody
	public Promocao setPromocao(@RequestBody Promocao promocao){
		ArrayList<Produto> produtos = new  ArrayList<Produto>();
		for(Produto produto: promocao.getProdutoList()) {
			produto = produtoRepositoty.findById(produto.getId()).get();
			produtos.add(produto);
		}
		promocao.setProdutoList(produtos);
		return promocaoRepositoty.save(promocao);
	}
}
