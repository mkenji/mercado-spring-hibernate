package com.itau.mercado.repository;

import org.springframework.data.repository.CrudRepository;
import com.itau.mercado.model.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

}
