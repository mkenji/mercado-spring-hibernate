package com.itau.mercado.repository;

import org.springframework.data.repository.CrudRepository;
import com.itau.mercado.model.Venda;

public interface VendaRepository extends CrudRepository<Venda, Integer>{

}
