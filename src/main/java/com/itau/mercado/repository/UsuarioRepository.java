package com.itau.mercado.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import com.itau.mercado.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	public Optional<Usuario> findByEmail(String email);

	public Object getUsuario(int i);
}
